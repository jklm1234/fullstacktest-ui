import { Component, ViewChildren, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { environment } from '../environments/environment';
declare var io: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild("baseChart") baseChart;
  title = 'ui';
  dataFromServer = [];
  dataArrivalTime = [];
  sessionStatus = "not_started";
  socket: any;
  currentNo;
  maxValuesInChart = 15;
  FlagValues = [];
  constructor() {

    // setTimeout(() =>{
    //   this.lineChartData[0].data.push(10);
    //   this.lineChartData[0].pointBackgroundColor= ["orange"];
    //   this.lineChartLabels.push("Feb");
    // },5000)
  }

  InsertToChart(data) {
    this.currentNo = data.randomNo;
    if (this.lineChartData[0].data.length >= this.maxValuesInChart) {

      console.log("removed this.lineChartData[0].pointBackgroundColor before ", this.lineChartData[0].pointBackgroundColor);

      this.lineChartData[0].data.splice(0, 1);
      this.lineChartData[0].pointBackgroundColor.splice(0, 1);
      this.lineChartLabels.splice(0, 1);
      console.log("removed this.lineChartData[0].pointBackgroundColor ", this.lineChartData[0].pointBackgroundColor);
    }
    // this.dataArrivalTime.push(data.time);
    setTimeout(() => {

      this.lineChartData[0].data.push(data.randomNo);
      if (data.randomNo === 7) {
        this.lineChartData[0].pointBackgroundColor.push("red");
        this.FlagValues.push(data);
        console.log("flag values array iss ", this.FlagValues);
      }
      else {
        this.lineChartData[0].pointBackgroundColor.push("black");
      }
      this.lineChartLabels.push("");
    }, 100)
  }
  StartSession() {
    this.sessionStatus = "started";
    this.ConnectToSocket();
  }
  StopSession() {
    this.socket.disconnect();
    // this.lineChartData[0].data = [];
    // this.lineChartData[0].pointBackgroundColor = [];
    // this.lineChartLabels = [];
    this.sessionStatus = "stopped";
  }
  ConnectToSocket() {
    this.socket = io(environment.sockerUrl);

    this.socket.on('connect', function () {
      console.log("connected ");
    });
    this.socket.on('heartrate', (data) => {
      this.dataFromServer.push(data);
      this.InsertToChart(data);

    });
    this.socket.on('disconnect', function () {
      console.log("disconnected ");
    });
  }
  public lineChartData: any[] = [
    { data: [], label: '', pointBackgroundColor: [] },
  ];
  public lineChartLabels: any[] = [];
  public lineChartOptions: ChartOptions = {
    responsive: true,
    elements: {
      line: {
        tension: 0 // disables bezier curves
      }
    },
    scales: {
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Values'
        },
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 15,
        }
      }],
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'time'
        }
      }]
    },
    legend: {
      display: false
    },
  };
  public lineChartColors: any[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public pointBorderColor = [{
    pointBackgroundColor: "red"
  }]
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

}
